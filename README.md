
## ASEQ - fast allele-specific studies from next-generation sequencing data

### DESCRIPTION
Single base level information from next-generation sequencing (NGS) allows for the quantitative assessment of biological phenomena such as mosaicism or allele-specific features in healthy and diseased cells. Such studies often present with computationally challenging burdens that hinder genome-wide investigations across large datasets that are now becoming available through the 1,000 Genomes Project and The Cancer Genome Atlas (TCGA) initiatives. ASEQ is a tool to perform gene-level allele-specific expression (ASE) analysis from paired genomic and transcriptomic NGS data without requiring paternal and maternal genome data. It offers an easy-to-use set of modes that transparently to the user takes full advantage of a built-in fast computational engine. ASEQ can be used to rapidly and reliably screen large NGS datasets for the identification of allele specific features and can be integrated in any NGS pipeline and runs on computer systems with multiple CPUs, CPUs with multiple cores or across clusters of machines.

### REFERENCES
Romanel A, Lago S, Prandi D, Sboner A, Demichelis F. *ASEQ: fast allele-specific studies from next-generation sequencing data*. BMC Medical Genomics 2015.

### INSTALL
Pre-compiled binaries are provided for Linux and Windows platforms.  
Source code compilation instructions for Linux, Windows and MacOSX platforms are provided in source files package (refer to INSTALL file).

### BASIC USAGE
Running ASEQ executable will list help and usage options.  
Detailed description of run options are listed in the Manual while usage examples are provided in Examples package (refer to README file).  
Pre-built annotation and input files required by ASEQ are provided in Annotation Files package.

### COPYRIGHT
Code by Alessandro Romanel    
Centre for Integrative Biology, University of Trento, Italy  
Email contact: alessandro.romanel@unitn.it 
ASEQ is distributed under the MIT Licence.
