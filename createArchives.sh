ASEQVERSION=1.1.11
tar czf ./current_packages/ASEQ-v$ASEQVERSION-linux64.tar.gz binaries/linux64
tar czf ./current_packages/ASEQ-v$ASEQVERSION-linux32.tar.gz binaries/linux32
tar czf ./current_packages/ASEQ-v$ASEQVERSION-win32.tar.gz binaries/win32
tar czf ./current_packages/ASEQ-v$ASEQVERSION-macosx.tar.gz binaries/macosx
tar czf ./current_packages/ASEQ-examples.tar.gz examples
tar czf ./current_packages/ASEQ-annotation-files.tar.gz annotations
cd ./include/samtools
make clean
cd ../../
tar czf ./current_packages/ASEQ-v$ASEQVERSION-source.tar.gz main.c DataStructures.h lib Makefile.linux Makefile.macosx Makefile.mingw Statistics.h ParseParameters.h Utilities.h include COPYING INSTALL Install-macosx.sh Install-linux.sh
