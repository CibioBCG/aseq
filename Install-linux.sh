#!/usr/bin/env bash

VERSION=$1

if [ $1 == i386 ]
  then
    echo "[i386]"
    echo "Compiling samtools..."
    cd ./include/samtools/
    make clean
    make
    mv libbam.a ../../lib/linux32/
    cd ../../
    echo "Compiling PaPI..."
    make -fMakefile.linux i386
    echo "Done."
fi

if [ $1 == x64 ]
  then
    echo "[i386]"
    echo "Compiling samtools..."
    cd ./include/samtools/
    make clean
    make
    mv libbam.a ../../lib/linux64/
    cd ../../
    echo "Compiling PaPI..."
    make -fMakefile.linux
    echo "Done."
fi

if [ $1 != i386 -a $1 != x64 ]
  then
    echo "Specify i386 or x64 architecture."
fi
