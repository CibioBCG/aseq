numargs=$#
pvcf=$1
pmbq=$2
pmrq=$3
pmdc=$4
pthreads=$5
pout=$6
for s in $(seq 7 $numargs)
do
    ASEQ bam=$7 vcf=$pvcf mbq=$pmbq mrq=$pmrq mdc=$pmdc threads=$pthreads out=$pout mode=PILEUP plight
    shift
done

