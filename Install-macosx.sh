#!/usr/bin/env bash

echo "[MacOSX]"
echo "Compiling samtools..."
cd ./include/samtools/
make clean
make
mv libbam.a ../../lib/macosx/
cd ../../
echo "Compiling ASEQ..."
make -fMakefile.macosx
echo "Done."
